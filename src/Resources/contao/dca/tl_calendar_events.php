<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2020 Leo Feyer
 *
 * @package   ZnrlIcsBundle
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2020
 */


/**
 * Table tl_calendar_events
 */

$GLOBALS['TL_DCA']['tl_calendar_events']['config']['onsubmit_callback'][] = array('Znrl\IcsBundle\IcsExport', 'exportCalendar');