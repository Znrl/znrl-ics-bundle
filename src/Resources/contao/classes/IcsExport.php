<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2020 Leo Feyer
 *
 * @package   ZnrlIcsBundle
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2020
 */


/**
 * Namespace
 */
namespace Znrl\IcsBundle;

use Sabre\VObject;
use Contao\File;
use Contao\DataContainer;
use Contao\CalendarEventsModel;
use Sabre\VObject\StringUtil;
use Sabre\VObject\DateTimeParser;

/**
 * Class IcsExport
 *
 * Export class for Contao calendars to .ics (ICal) files.
 * @copyright  Lorenz Ketterer 2020
 * @author     Lorenz Ketterer <lorenz.ketterer@web.de>
 */

class IcsExport extends \Backend
{
    /**
     * The ID of the Calendar we want to export (PID from Calendar Event where Callback was triggered)
     * @var string
     */
    public $calId;

    public function __construct()
    {
        // Include composer autoloader for Sabre\Vobject (library to deal with Ical files)
        include TL_ROOT.'/composer/vendor/autoload.php';
    }

    /**
     * Function gets triggert by onsubmit_callback in tl_calendar_files and initiates the export(s)
     * @param DataContainer $dc
     */
    public function exportCalendar(DataContainer $dc)
    {
        $this->calId = $dc->activeRecord->pid;

        $icsExports = IcsModel::findBy(array('type=?', 'calendar=?', 'export_once!=?'), array('export', $this->calId, '1'));

        foreach ($icsExports as $icsExport) {
                $this->createCalendar($icsExport);
        }
    }

    /**
     * Function gets triggert by onsubmit_callback in tl_znrl_ics by submitting any export rule and initiates the export
     * @param DataContainer $dc
     */
    public function exportCalendarOnce(DataContainer $dc)
    {
        $this->calId = $dc->activeRecord->calendar;
        $icsExport = $dc->activeRecord;
        $this->createCalendar($icsExport);

    }

    /**
     * Creates a VCalendar object initiates Event adding and .ics file creation.
     * @param array $icsExport
     */
    protected function createCalendar($icsExport)
    {
        $vcalendar = new VObject\Component\VCalendar();

        if ($icsExport->export_unpublished == 1) {
            $calEvents = CalendarEventsModel::findBy(array('pid=?'), array($this->calId));
        }
        else {
            $calEvents = CalendarEventsModel::findBy(array('pid=?', 'published=?'), array($this->calId, '1'));
        }


        foreach ($calEvents as $calEvent) {
			if ($icsExport->add_valarm){
				switch ($icsExport->valarm_time_unit) {
                case 'minutes':
                    $durval = 'M';
                    break;
                case 'hours':
                    $durval = 'H';
                    break;
                case 'days':
                    $durval = 'D';
                    break;
            }
				$duration = '-P'.$icsExport->valarm_time.$durval;
				$valarm = $vcalendar->createComponent('VALARM');
				$valarm->DESCRIPTION = $icsExport->valarm_description;
				$valarm->ACTION = 'DISPLAY';
				$valarm->add($vcalendar->createProperty('TRIGGER', $duration, ['VALUE' => 'DURATION']));
			}
            $this->createEvent($vcalendar, $calEvent, $valarm);
        }

        $icsContent = $vcalendar->serialize();

        // Might be unecessary
        if(!StringUtil::isUTF8($icsContent)) {
             $icsContent = StringUtil::convertToUTF8($icsContent);
        }
        $this->createIcsFile($icsExport, $icsContent);
    }

    /**
     * Creates and adds Events to the VCalendar object.
     * @param object $vcalendar
     * @param array $calEvent
     * @return object $vcalendar
     */
    protected function createEvent($vcalendar, $calEvent, $valarm = NULL)
    {

        // Just use time values since contao always sets them, endDate is not always set.

        $vevent = $vcalendar->createComponent('VEVENT');

        $vevent->SUMMARY = $calEvent->title;
        $vevent->DTSTART = new \DateTime(\Date::parse('Y-m-d H:i:s', $calEvent->startTime));
        $vevent->DTEND = new \DateTime(\Date::parse('Y-m-d H:i:s', $calEvent->endTime));

        if ($calEvent->location != null) {
            $vevent->LOCATION = $calEvent->location;
        }

        if ($calEvent->teaser != null) {
            $vevent->DESCRIPTION = $this->htmlToText($calEvent->teaser);
        }

        if ($calEvent->recurring == 1) {


            $arrRep = unserialize($calEvent->repeatEach);

            switch ($arrRep['unit']) {
                case 'days':
                    $freq = 'DAILY';
                    break;
                case 'weeks':
                    $freq = 'WEEKLY';
                    break;
                case 'months':
                    $freq = 'MONTHLY';
                    break;
                case 'years':
                    $freq = 'YEARLY';
                    break;
            }


            $interval = $arrRep['value'];
            $until = \Date::parse('Ymd\\THis\\Z', $calEvent->repeatEnd);
            $count = $calEvent->recurrences;



            $vevent->RRULE = 'FREQ='.$freq.';INTERVAL='.$interval.';UNTIL='.$until.';COUNT='.$count;
        }
		
		if ($valarm) {
			$vevent->add($valarm);
		}

        $vcalendar->add($vevent);

        return $vcalendar;
    }

    /**
     * Creates and writes .ics File.
     * @param array $icsExport
     * @param string $icsContent
     */
    protected function createIcsFile($icsExport, $icsContent)
    {
        $path = \FilesModel::findByUuid($icsExport->file_destination_files)->path;
        $dir = $path.'/';
        
        $icsfile = new File($dir.$icsExport->filename.'.ics');
        $icsfile->write($icsContent);
        $icsfile->close();
    }
    
     /**
     * Converts the html into text.
     * @param string $html
     */
    function htmlToText($html) {
        // preserve links
        $html = preg_replace('/<a href="(.*)">/', '$1 ', $html);
        // remove HTML TAGs
        $text = preg_replace ('/<[^>]*>/', ' ', $html);
     
        // remove control characters
        $text = str_replace("\r", '', $text);    // --- replace with empty space
        // $text = str_replace("\n", ' ', $text);   // --- replace with space, this would remove line breaks
        $text = str_replace("\t", ' ', $text);   // --- replace with space
        
        $text = html_entity_decode($text);
        // remove multiple spaces
        $text = trim(preg_replace('/ {2,}/', ' ', $text));
     
        return $text; 
    }
}
