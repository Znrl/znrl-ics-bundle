<?php

/**
 * Contao Open Source CMS
*
* Copyright (c) 2005-2020 Leo Feyer
*
* @package   ZnrlIcsBundle
* @author    Lorenz Ketterer <lorenz.ketterer@web.de>
* @license   GNU/LGPL
* @copyright Lorenz Ketterer 2020
*/


/**
 * Namespace
*/
namespace Znrl\IcsBundle;


/**
 * Class IcsDca
 *
 * Helper Class for DCA
 * @copyright  Lorenz Ketterer 2020
 * @author     Lorenz Ketterer <lorenz.ketterer@web.de>
 */

class IcsDca extends \Backend
{
    /**
     * Gets Calendars for options_callback so the calendar which will get exported can be chosen
     * @return array
     */
    public function getCalendars()
    {
        $this->import('BackendUser', 'User');
        if (!$this->User->isAdmin && !is_array($this->User->calendars))
        {
            return array();
        }
        $arrCalendars = array();
        $objCalendars = $this->Database->execute("SELECT id, title FROM tl_calendar ORDER BY title");
        while ($objCalendars->next())
        {
            if ($this->User->hasAccess($objCalendars->id, 'calendars'))
            {
                $arrCalendars[$objCalendars->id] = $objCalendars->title;
            }
        }
        return $arrCalendars;
    }

    /**
     * Triggered by onsubmit_callback export calendar if saved manually
     * @return
     */
    public function exportCalendarOnSave($dc)
    {
        if (\Input::post('SUBMIT_TYPE') != 'auto') {
            $export = new IcsExport();
            $export->exportCalendarOnce($dc);
        }
        else {
            return;
        }
    }
}