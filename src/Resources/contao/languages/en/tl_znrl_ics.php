<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2020 Leo Feyer
 *
 * @package   ZnrlIcsBundle
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2020
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_znrl_ics']['title'] = array('Title', 'Please enter a title.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['type'] = array('Type', 'Please choose what type of calendar job it is.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['type']['export'] = 'Export';
$GLOBALS['TL_LANG']['tl_znrl_ics']['export_once'] = array('Export Once', 'Normaly the Calender will be exported every time a calendar event is saved.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['calendar'] = array('Calendar', 'Please choose a calendar.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['export_unpublished'] = array('Export unpublished events', 'Please choose if unbuplished events should be exported.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['filename'] = array('Filename', 'Please insert a filename WITHOUT ".ics" filetype.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['file_destination_files'] = array('Path in files-directory', 'Choose the destination within files-directory.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['add_valarm'] = array('Add notification', 'Adds a notofocation (VALARM) to every event.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['valarm_time'] = array('Time', 'Insert how long before the event you want receive a notification.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['valarm_time_unit'] = array('Time unit', 'Choose the time Unit.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['valarm_time_unit']['minutes'] = 'minute(s)';
$GLOBALS['TL_LANG']['tl_znrl_ics']['valarm_time_unit']['hours'] = 'hour(s)';
$GLOBALS['TL_LANG']['tl_znrl_ics']['valarm_time_unit']['days'] = 'day(s)';
$GLOBALS['TL_LANG']['tl_znrl_ics']['valarm_description'] = array('Description', 'Description for the notification.');


/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_znrl_ics']['title_legend'] = 'Title and type';
$GLOBALS['TL_LANG']['tl_znrl_ics']['export_legend'] = 'Calendar and file';
$GLOBALS['TL_LANG']['tl_znrl_ics']['advanced_legend'] = 'Advanced';


/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_znrl_ics']['new']    = array('New', 'Create a new calendar export');
$GLOBALS['TL_LANG']['tl_znrl_ics']['show']   = array('Details', 'Show the details of ID %s');
$GLOBALS['TL_LANG']['tl_znrl_ics']['edit']   = array('Edit', 'Edit ID %s');
$GLOBALS['TL_LANG']['tl_znrl_ics']['cut']    = array('Move', 'Move ID %s');
$GLOBALS['TL_LANG']['tl_znrl_ics']['copy']   = array('Duplicate', 'Duplicate ID %s');
$GLOBALS['TL_LANG']['tl_znrl_ics']['delete'] = array('Delete', 'Delete ID %s');
