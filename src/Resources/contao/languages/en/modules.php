<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2020 Leo Feyer
 *
 * @package   ZnrlIcsBundle
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2020
 */


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['znrl_ics'] = array('Calendar -> Ics', 'Export Calendar to ics (ICal)');
