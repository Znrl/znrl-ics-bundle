<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2020 Leo Feyer
 *
 * @package   ZnrlIcsBundle
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2020
 */


/**
 * BACK END MODULES
 */

$GLOBALS['BE_MOD']['content']['znrl_ics'] = array(
    'tables'     => array('tl_znrl_ics'),
    'icon'       => 'system/modules/znrl_ics/assets/ics-import-export.png'
);


/**
 * MODEL MAPPINGS
 */

$GLOBALS['TL_MODELS']['tl_znrl_ics'] = 'Znrl\IcsBundle\IcsModel';
