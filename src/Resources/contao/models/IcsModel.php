<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2020 Leo Feyer
 *
 * @package   ZnrlIcsBundle
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2020
 */


/**
 * Namespace
 */

namespace Znrl\IcsBundle;


/**
 * Model IcsModel
 *
 * @copyright  Lorenz Ketterer 2020
 * @author     Lorenz Ketterer <lorenz.ketterer@web.de>
 */


class IcsModel extends \Model
{
    protected static $strTable = 'tl_znrl_ics';
}