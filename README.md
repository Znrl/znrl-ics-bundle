# README #

### What is it? ###

This is a Contao extension to export Contao-calendars

* Right now it only exports complete (no added content elements) calendars to ICal (.ics) files.
* Calendars get exported by saving the export rule created by this extension (every time).
* If it is no single export (export once) every time a event of a calendar with an export rule gets saved it gets exported.
* Event title is used as SUMMARY and teaser is used as DESCRIPTION.
* It exports Title, Start Date/Time, End Date/Time, Location, Teaser and handles Recurrence settings (not well tested).


### Additional Information? ###

This extension uses Sabre\Vobject
https://github.com/fruux/sabre-vobject
